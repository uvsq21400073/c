#ifndef VECTEUR_H
#define VECTEUR_H
#include<iostream>
#include<list>

class Vecteur
{
    private:
        std::list<int> tab;
        int taille;
    public:
        Vecteur();
        int operator [](int i);
        int& operator [] (int i) const;
        std::list<int> getTab() const;
        Vecteur& operator +(const Vecteur&);
        Vecteur& operator +=(const Vecteur&);
        //friend std::ostream operator<<(std::ostream flux const);

        virtual ~Vecteur();

};
#endif // VECTEUR_H

