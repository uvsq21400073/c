#ifndef CSTRING_H
#define CSTRING_H

#include <iostream>
#include <cstring>

using namespace std;

class CString
{
    public:
        CString();
        CString(string chaine);
        CString(char lettre);
        static int nbrChaines;
        string getString();
        CString plus(char lettre);
        bool plusGrandQue(CString s1);
        bool infOuEgale(CString s1);
        CString plusGrand(CString s1);

        virtual ~CString();
    private:
        string chaines;
};
#endif // CSTRING_H
