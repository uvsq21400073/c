#ifndef DEFINITION_H
#define DEFINITION_H

class Definition
{
    public:
        Definition(std::string mot, std::string definition);
        std::string getDef();
        std::string getClef();
        virtual ~Definition();

    private:
        std::string mot;
        std::string definition;
};

#endif // DEFINITION_H
