#include <iostream>
#include <cmath>
#include "point.h"
using namespace std;

class Segment
{
	private:
		Point p,q;
	public:
		Segment(Point p, Point q);
		~Segment();
		Point getP();
		Point getQ();

		int longueur()
		{
			int longueur;
			longueur = sqrt((q.getX() - p.getX())*(q.getX() - p.getX()) + (q.getY() - p.getY())*(q.getY() - p.getY()));
			return longueur;
		}

		bool estVertical ()
		{
			if (p.getX() == q.getX()) return true;
			else return false;
		}

		bool estHorizontal ()
		{
			if (p.getY() == q.getY()) return true;
			else return false;
		}

		bool estSurDiagonale ()
		{
			if ((p.getX() != q.getX()) && (p.getY() != q.getY())) return true;
			else return false;
		}

		bool estCroise (const Segment& S)
		{
			int a1, a2;
			a1 = (p.getY() - q.getY()) / (p.getX() - q.getX());
			a2 = ((S.p.getY() - S.q.getY()) / (S.p.getX() - S.q.getX()));

			if (a1 - a2 == 0) return true;
			else return false;
		}
};

Segment:: Segment(Point c_p, Point c_q)
		{
			p = c_p;
			q = c_q;
		}

Segment:: ~Segment()
		{

		}

Point Segment:: getP()
				{
					return p;
				}
Point Segment:: getQ()
				{
					return q;
				}
