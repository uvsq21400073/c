#include <iostream>
using namespace std;

class Point
{
	private:
		int x,y;
	public:
		Point();
		Point(int c_x, int c_y);
		Point(const Point& p);
		~Point();
		int getX() const;
		int getY() const;

		void afficher() const
		{
			cout << "La coordonnee x du point est: " << x << endl;
			cout << "La coordonnee y du point est: " << y << endl;
		}

		void cloner(const Point& p)
		{
			x = p.getX();
			y = p.getY();
		}
};

Point::	Point()
		{
			x = 0;
			y = 0;
		}
Point::	Point(int c_x, int c_y)
		{
			x = c_x;
			y = c_y;
		}
Point::	Point(const Point& p)
		{
			x = p.getX();
			y = p.getY();
		}
Point::	~Point()
		{

		}

int Point:: getX() const
		{
			return x;
		}

int Point:: getY() const
		{
			return y;
		}
