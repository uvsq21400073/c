#include <iostream>
#include "segment.h"
using namespace std;

int main ()
{
	Point p1 (10,10);
	Point p2 (20,20);
	Segment p(p1, p2);

	Point q1 (20,20);
	Point q2 (10,10);
	Segment q(q1,q2);

	if (p.estCroise(q) == true)
	{
		cout << "Les diagonales se croisent" <<endl;
	}
	else
	{
		cout << "Les diagonales ne se croisent pas" <<endl;
	}

	return 0;
}
