#include <iostream>
using namespace std;

void tableauEcriture(int tab[], int taille)
{
	int i;
	for (i = 0; i < taille; i++)
	{
		tab[i] = i;
	}
	
}

void tableauLecture(const int tab[], int taille)
{
	int i;
	for (i = 0; i < taille; i++)
	{
		cout << "La valeur du tableau a l'indice " << i << " est "<< tab[i] << endl;
	}
}

int main ()
{
	int tab[]= {42,69,2,5,32,45,15,2,8,4};
	tableauLecture(tab,3);
	tableauEcriture(tab,3);
	tableauLecture(tab,3);

	return 0;
}
