#include <iostream>
using namespace std;

void echange (int& x, int& y)
{
	int tmp = x;
	x = y;
	y = tmp;
}

int main ()
{
	int x = 10;
	int y = 20;
	echange(x,y);
	cout <<"La valeur de x est "<<x<<" et la valeur de y est "<<y<<"\n";
	
	return 0;
}
