#ifndef POINT_H
#define POINT_H


class Point
{
    private:
		int x,y;
	public:
		Point();
		Point(int c_x, int c_y);
		Point(const Point& p);
		~Point();
		int getX() const;
		int getY() const;
		void modifX(int n);
		void modifY(int n);
		void afficher() const;
		void cloner(const Point& p);
};

#endif // POINT_H
