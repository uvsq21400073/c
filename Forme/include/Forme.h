#ifndef FORME_H
#define FORME_H
#include "Point.h"

class Forme
{
    public:
        Forme();
        virtual ~Forme();
        virtual int getNb_Sommets() = 0;
        virtual void affichage() = 0;
        virtual void deplacement(int abscisse, int ordonnee) = 0;
};

#endif // FORME_H
