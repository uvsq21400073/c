#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "Forme.h"


class Rectangle : public Forme
{
    public:
        Rectangle();
        Rectangle(Point A, Point C);
        int getNb_Sommets();
        void affichage();
        void deplacement(int abscisse, int ordonnee);
        virtual ~Rectangle();

    private:
        Point a;
        Point c;
};

#endif // RECTANGLE_H
