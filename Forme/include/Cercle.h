#ifndef CERCLE_H
#define CERCLE_H

#include "Forme.h"


class Cercle : public Forme
{
    public:
        Cercle();
        Cercle(Point P, int Rayon);
        int getNb_Sommets();
        void affichage();
        void deplacement(int abscisse, int ordonnee);
        virtual ~Cercle();

    protected:

    private:
        Point p;
        int rayon;
};

#endif // CERCLE_H
