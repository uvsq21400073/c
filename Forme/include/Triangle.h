#ifndef TRIANGLE_H
#define TRIANGLE_H
#include "Segment.h"


class Triangle: public Forme
{
    public:
        Triangle();
        Triangle(Point A, Point B, Point C);
        int getNb_Sommets();
        void affichage();
        void deplacement(int abscisse, int ordonnee);
        virtual ~Triangle();

    private:
        Point a;
        Point b;
        Point c;
};

#endif // TRIANGLE_H
