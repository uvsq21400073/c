#ifndef SEGMENT_H
#define SEGMENT_H
#include "Point.h"
#include "Forme.h"

class Segment: public Forme
{
    private:
		Point p,q;
	public:
	    Segment();
		Segment(Point p, Point q);
		~Segment();
		Point getP();
		Point getQ();
		int longueur();
		bool estVertical ();
		bool estHorizontal ();
		bool estSurDiagonale ();
		int getNb_Sommets();
		void affichage();
		void deplacement(int abscisse, int ordonnee);
};

#endif // SEGMENT_H
