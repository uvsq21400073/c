#include <iostream>
#include "Segment.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "Cercle.h"

using namespace std;

int main()
{
    Cercle c;
    Rectangle r;
    Segment s;
    Triangle t;

    c.affichage();
    c.deplacement(10,10);
    c.affichage();

    return 0;
}
