#include "Rectangle.h"
#include <cstdlib>
#include <iostream>

using namespace std;

Rectangle::Rectangle(): a(10,10), c(20,20){}

Rectangle::Rectangle(Point A, Point C)
{
    a = A;
    c = C;
}

int Rectangle::getNb_Sommets()
{
    return 4;
}

void Rectangle::affichage()
{
    cout <<"Le rectangle a pour coordonnees "<<endl;
    cout <<"La coordonnee en x du point a est "<< a.getX() <<" et sa coordonnee en y est "<< a.getY() << endl;
    cout <<"La coordonnee en x du point b est "<< c.getX() <<" et sa coordonnee en y est "<< a.getY() << endl;
    cout <<"La coordonnee en x du point c est "<< c.getX() <<" et sa coordonnee en y est "<< c.getY() << endl;
    cout <<"La coordonnee en x du point d est "<< a.getX() <<" et sa coordonnee en y est "<< c.getY() << endl;
    cout <<endl;
}

void Rectangle::deplacement(int abscisse, int ordonnee)
{
    a.modifX(abscisse);
    a.modifY(ordonnee);

    c.modifX(abscisse);
    c.modifY(ordonnee);
}

Rectangle::~Rectangle()
{
    //dtor
}
