#include <iostream>
#include "Point.h"

using namespace std;

Point::	Point()
		{
			x = 0;
			y = 0;
		}
Point::	Point(int c_x, int c_y)
		{
			x = c_x;
			y = c_y;
		}
Point::	Point(const Point& p)
		{
			x = p.getX();
			y = p.getY();
		}
Point::	~Point()
		{

		}

int Point:: getX() const
		{
			return x;
		}

int Point:: getY() const
		{
			return y;
		}

void Point::modifX(int n)
{
    x += n;
}

void Point::modifY(int n)
{
    y += n;
}

void Point:: afficher() const
		{
			cout << "La coordonnee x du point est: " << x << endl;
			cout << "La coordonnee y du point est: " << y << endl;
		}

void Point::cloner(const Point& p)
{
    x = p.getX();
    y = p.getY();
}
