#include "Segment.h"
#include <cmath>
#include <iostream>

using namespace std;

Segment::Segment(): p(10,10), q(20,20){}

Segment::Segment(Point c_p, Point c_q)
{
    p = c_p;
    q = c_q;
}

Segment::~Segment()
{

}

Point Segment::getP()
{
    return p;
}
Point Segment:: getQ()
{
    return q;
}
int Segment::longueur()
{
    int longueur;
    longueur = sqrt((q.getX() - p.getX())*(q.getX() - p.getX()) + (q.getY() - p.getY())*(q.getY() - p.getY()));
    return longueur;
}

bool Segment:: estVertical ()
{
    if (p.getX() == q.getX()) return true;
    else return false;
}

bool Segment:: estHorizontal ()
{
    if (p.getY() == q.getY()) return true;
    else return false;
}

bool Segment::estSurDiagonale ()
{
    if ((p.getX() != q.getX()) && (p.getY() != q.getY())) return true;
    else return false;
}

int Segment::getNb_Sommets()
{
    return 2;
}

void Segment::affichage()
{
    cout <<"Le segment a pour coordonnees: "<<endl;
    cout <<"La coordonnee en x du point p est "<< p.getX() <<" et sa coordonnee en y est "<< p.getY() << endl;
    cout <<"La coordonnee en x du point q est "<< q.getX() <<" et sa coordonnee en y est "<< q.getY() << endl;
    cout <<endl;
}

void Segment::deplacement(int abscisse, int ordonnee)
{
    p.modifX(abscisse);
    p.modifY(ordonnee);

    q.modifX(abscisse);
    q.modifY(ordonnee);
}
