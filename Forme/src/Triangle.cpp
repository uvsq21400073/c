#include "Triangle.h"
#include <iostream>

using namespace std;

Triangle::Triangle(): a(10,10),b(15,15),c(10,20){}


Triangle::Triangle(Point A, Point B, Point C)
{
    a = A;
    b = B;
    c = C;
}

int Triangle::getNb_Sommets()
{
    return 3;
}

void Triangle::affichage()
{
    cout <<"Le triangle a pour coordonnees: "<<endl;
    cout <<"La coordonnee en x du point a est "<< a.getX() <<" et sa coordonnee en a est "<< a.getY() << endl;
    cout <<"La coordonnee en x du point b est "<< b.getX() <<" et sa coordonnee en b est "<< b.getY() << endl;
    cout <<"La coordonnee en x du point c est "<< c.getX() <<" et sa coordonnee en c est "<< c.getY() << endl;
    cout<<endl;
}

void Triangle::deplacement(int abscisse, int ordonnee)
{
    a.modifX(abscisse);
    a.modifY(ordonnee);

    b.modifX(abscisse);
    b.modifY(ordonnee);

    c.modifX(abscisse);
    c.modifY(ordonnee);
}

Triangle::~Triangle()
{
    //dtor
}
