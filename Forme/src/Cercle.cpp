#include "Cercle.h"
#include <iostream>

using namespace std;

Cercle::Cercle(): p(10,10),rayon(15){}

Cercle::Cercle(Point P, int Rayon)
{
    p = P;
    rayon = Rayon;
}

int Cercle::getNb_Sommets()
{
    return 1;
}

void Cercle::affichage()
{
    cout << "Le cercle a pour rayon "<< rayon << " et les coordonnees de son centre sont "<< p.getX() << " et "<< p.getY()<<endl;
}

void Cercle::deplacement(int abscisse, int ordonnee)
{
    p.modifX(abscisse);
    p.modifY(ordonnee);
}

Cercle::~Cercle()
{
    //dtor
}
